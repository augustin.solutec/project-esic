-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 27 Mars 2020 à 15:57
-- Version du serveur :  10.1.44-MariaDB-0+deb9u1
-- Version de PHP :  7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `user`
--
CREATE DATABASE `users`;
CREATE USER admin@localhost IDENTIFIED BY 'motdepass';
GRANT ALL PRIVILEGES ON users.* TO admin@localhost;
FLUSH PRIVILEGES;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--
USE users;
CREATE TABLE `utilisateurs` (
  `ID` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `date_inscription` date NOT NULL,
  `date_sortie` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `utilisateurs`
--

ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`ID`);





